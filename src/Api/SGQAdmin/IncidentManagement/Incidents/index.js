import { defaultResponseError } from '../../../Commons/constants'
import { BASE_INCIDENT_MANAGEMENT_URL } from '../constants'

const INCIDENTS_PATH = 'incidents'

const URL = `${BASE_INCIDENT_MANAGEMENT_URL}/${INCIDENTS_PATH}`

export const options = {
  headers: { 'Content-Type': 'application/json' }
}

export const fetchIncidents = async (token, id) => {
  if(!token){
    return defaultResponseError
  }
  options.method = 'GET'
  options.headers['x-access-tokens'] = token
  options.headers['Cache-Control'] = 'no-cache'
  let REQUEST_URL = URL
  if(id){
    REQUEST_URL = `${URL}/${id}`
  }
  return fetch(REQUEST_URL, options)
    .then((response) => response.json())
    .then((response) => {
      return response
    })
    .catch((error) => {
      const customError = defaultResponseError
      customError.message = error
      return error
    })
}

export const createOrUpdateIncident = async (token, data) => {
  if(!token){
    return defaultResponseError
  }
  if(data.uuid){
    options.method = 'PUT'
  }else{
    options.method = 'POST'
  }
  
  options.headers['x-access-tokens'] = token
  options.body = JSON.stringify({
    ...data
  })
  return fetch(URL, options)
    .then((response) => {
      return {}
    })
    .catch((error) => {
      const customError = defaultResponseError
      customError.message = error
      return error
    })
}

export const aproveIncident = async (token, data) => {
  if(!token){
    return defaultResponseError
  }
  options.method = 'PATCH'
  options.headers['x-access-tokens'] = token
  options.body = JSON.stringify({
    ...data
  })
  return fetch(URL, options)
    .then((response) => {
      return {}
    })
    .catch((error) => {
      const customError = defaultResponseError
      customError.message = error
      return error
    })
}

export const deleteIncident = async (token, id) => {
  if(!token){
    return defaultResponseError
  }
  options.method = 'DELETE'
  options.headers['x-access-tokens'] = token
  let REQUEST_URL = URL
  if(id){
    REQUEST_URL = `${URL}/${id}`
  }
  return fetch(REQUEST_URL, options)
    .then((response) => {
      return {}
    })
    .catch((error) => {
      const customError = defaultResponseError
      customError.message = error
      return error
    })
}
