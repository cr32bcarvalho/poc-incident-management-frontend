import { defaultResponseError } from '../../../Commons/constants'
import { BASE_INCIDENT_MANAGEMENT_URL } from '../constants'

const URL = `${BASE_INCIDENT_MANAGEMENT_URL}`


export const options = {
  method: 'GET',
  headers: { 'Content-Type': 'application/json' }
}

export const fetchDashboard = async (token) => {

  if(!token){
    return defaultResponseError
  }
  options.headers['x-access-tokens'] = token
  options.headers['Cache-Control'] = 'no-cache'
  return fetch(URL, options)
    .then((response) => response.json())
    .then((response) => {
      return response
    })
    .catch((error) => {
      const customError = defaultResponseError
      customError.message = error
      return error
    })
}