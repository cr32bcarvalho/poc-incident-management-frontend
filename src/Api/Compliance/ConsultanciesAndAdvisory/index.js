import { defaultResponseError } from '../../Commons/constants'
import { BASE_COMPLIANCE_URL } from '../constants'

const CONSULTANCY_AND_ADVISORY_PATH = 'consultancies-and-advisory'

const BASE_URL = `${BASE_COMPLIANCE_URL}${CONSULTANCY_AND_ADVISORY_PATH}`


export const options = {
  method: 'GET',
  headers: { 'Content-Type': 'application/json' }
}

export const fetchConsults = async (uuid) => {

  let URL = BASE_URL

  if(uuid){
    URL = `${URL}/${uuid}` 
  }

  return fetch(URL, options)
    .then((response) => response.json())
    .then((response) => {
      return response
    })
    .catch((error) => {
      const customError = defaultResponseError
      customError.message = error
      return error
    })
}