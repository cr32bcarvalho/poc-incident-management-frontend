import { defaultResponseError } from '../../Commons/constants'
import { BASE_COMPLIANCE_URL } from '../constants'

const STANDARDS_AND_REGULATIONS_PATH = 'standards-and-regulations'
const ASSOCIATIONS = 'associations'
const STANDARDS = 'standards'
const REQUIREMENTS = 'requirements'
const BASE_URL = `${BASE_COMPLIANCE_URL}${STANDARDS_AND_REGULATIONS_PATH}`


export const options = {
  method: 'GET',
  headers: { 'Content-Type': 'application/json' }
}

export const fetchAssociations = async (uuid) => {

  let URL = `${BASE_URL}/${ASSOCIATIONS}`

  if(uuid){
    URL = `${URL}/${uuid}` 
  }

  return fetch(URL, options)
    .then((response) => response.json())
    .then((response) => {
      return response
    })
    .catch((error) => {
      const customError = defaultResponseError
      customError.message = error
      return error
    })
}

export const fetchStandards = async (association_uuid, uuid) => {

  if(!association_uuid){
    return defaultResponseError
  }

  let URL = `${BASE_URL}/${STANDARDS}/${association_uuid}`

  if(uuid){
    URL = `${URL}/${uuid}` 
  }

  return fetch(URL, options)
    .then((response) => response.json())
    .then((response) => {
      return response
    })
    .catch((error) => {
      const customError = defaultResponseError
      customError.message = error
      return error
    })
}

export const fetchRequirements = async (standard_uuid, uuid) => {

  if(!standard_uuid){
    return defaultResponseError
  }

  let URL = `${BASE_URL}/${REQUIREMENTS}/${standard_uuid}`

  if(uuid){
    URL = `${URL}/${uuid}` 
  }

  return fetch(URL, options)
    .then((response) => response.json())
    .then((response) => {
      return response
    })
    .catch((error) => {
      const customError = defaultResponseError
      customError.message = error
      return error
    })
}