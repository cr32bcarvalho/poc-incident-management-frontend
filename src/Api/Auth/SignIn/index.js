import { BASE_AUTH_URL } from '../constants'
import { authError } from '../../Commons/constants'

const AUTH_PATH = 'sign-in'
const LOGIN_URL = `${BASE_AUTH_URL}/${AUTH_PATH}`

export const options = {
  method: 'POST',
  headers: { 'Content-Type': 'application/json' },
  body: null
}

export const signIn = async (user) => {
  const {email, password} = await user

  if(!email || !password){
    return authError
  }
  options.body = JSON.stringify({
    username: email,
    password
  })
  return fetch(LOGIN_URL, options)
    .then((response) => response.json())
    .then((response) => {
      return response
    })
    .catch((error) => {
      const customError = authError
      customError.message = error
      return error
    })
}