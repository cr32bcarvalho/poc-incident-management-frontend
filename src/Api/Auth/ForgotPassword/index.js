import { BASE_AUTH_URL } from '../constants'
import { defaultResponseError } from '../../Commons/constants'

const FORGOT_PASSWORD_PATH = 'forgot-password'
const URL = `${BASE_AUTH_URL}/${FORGOT_PASSWORD_PATH}`


export const options = {
  method: 'POST',
  headers: { 'Content-Type': 'application/json' },
  body: null
}

export const forgotPassword = async (username) => {

  if(!username){
    return defaultResponseError
  }

  options.body = JSON.stringify({
    username: username
  })

  return fetch(URL, options)
    .then((response) => response.json())
    .then((response) => {
      return response
    })
    .catch((error) => {
      const customError = defaultResponseError
      customError.message = error
      return error
    })
}