import { BASE_AUTH_URL } from '../constants'
import { defaultResponseError } from '../../Commons/constants'

const RESET_PASSWORD_PATH = 'confirm-forgot-password'
const URL = `${BASE_AUTH_URL}/${RESET_PASSWORD_PATH}`


export const options = {
  method: 'POST',
  headers: { 'Content-Type': 'application/json' },
  body: null
}

export const resetPassword = async (data) => {

  if(!data.username || !data.password || !data.code){
    return defaultResponseError
  }

  options.body = JSON.stringify({
    username: data.username,
    password: data.password,
    code: data.code
  })

  return fetch(URL, options)
    .then(response => response.json())
    .then(response => {
      return response
    })
    .catch((error) => {
      const customError = defaultResponseError
      customError.message = error
      return error
    })
}