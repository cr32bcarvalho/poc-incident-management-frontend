
import { INVALIDATE, REQUEST, RECEIVE, ERROR } from './constants'

const initialState = {
  isFetching: false,
  didInvalidate: false,
  data: null,
  error: null,
  lastUpdated: null
}

const reduce = (state = initialState, action) => {
  switch (action.type) {
    case INVALIDATE:
      return initialState
    case REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        didInvalidate: false,
        data: action.data,
        error: null
      })
    case ERROR:
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        error: action.error,
        lastUpdated: action.recievedAt
      })
    case RECEIVE:
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        data: action.data,
        lastUpdated: action.recievedAt
      })
    default:
      return state
  }
}

export const resetPassword = (state = initialState, action) => {
  switch (action.type) {
    case INVALIDATE:
    case RECEIVE:
    case REQUEST:
    case ERROR:
      return Object.assign({}, state, reduce(state, action))
    default:
      return state
  }
}
