import React, { Component } from 'react'
import { connect } from 'react-redux'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import TextField from '@material-ui/core/TextField'
import Link from '@material-ui/core/Link'
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'
import Container from '@material-ui/core/Container'
import Paths from '../../../Router/paths'
import Loading from '../../../Commons/Components/Loading'

import {
  fetchDataIfNeeded,
  invalidate
} from './Redux/actions'
import { withStyles } from '@material-ui/core/styles'
import { styles } from '../Commons/styles'
import Copyright from '../../../Commons/Components/Copyright'

class ResetPassword extends Component {
  constructor(props) {
    super(props)
    this.state = {
      formSubmited: false,
      username: '',
      code: ''
    }

    const { dispatch } = this.props
    dispatch(invalidate({}))
  }

  componentDidUpdate(prevProps) {
    const { data, error, isFetching, dispatch } = this.props
    if (
      this.state.formSubmited &&
      !isFetching &&
      (data !== null || error !== null)
    ) {
      this.setState({ formSubmited: false })
      if (data != null) {
        console.log(`Data: ${data}`)
      }
    } else if (this.state.formSubmited) {
      this.setState({ formSubmited: false })
      dispatch(invalidate({}))
    }
  }

  setTextHandler = (key, value) => {
    if(this.props.error){
      this.props.dispatch(invalidate({}))
    }
    this.setState({
      [key]: value
    })
  }

  handleSubmit = (event) => {
    event.preventDefault()
    const { username, password, passwordConfirm, code } = this.state
    const { dispatch } = this.props
    
    if(password != passwordConfirm){
      return
    }
    const data = {
      username,
      password, 
      code
    }
    dispatch(fetchDataIfNeeded(data))
    this.setState({ formSubmited: true })
  }
  
  render(){
    const { classes, error } = this.props
    return (
      <Container  component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Typography component="h1" variant="h5" >
            {'Reset password?'}
          </Typography>
          
          <form className={classes.form} noValidate onSubmit={this.handleSubmit}>
            {error && (
                <>
                  <CssBaseline />
                  <Typography component="p" align="center" color="error" >
                    {error.message}
                  </Typography>
                </>
            )}
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              onChange={(event) => this.setTextHandler(event.target.name, event.target.value)}
              id="email"
              label="Email Address"
              name="username"
              autoComplete="email"
              autoFocus
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              onChange={(event) => this.setTextHandler(event.target.name, event.target.value)}
              id="code"
              label="Code"
              name="code"
              autoFocus
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              onChange={(event) => this.setTextHandler(event.target.name, event.target.value)}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="passwordConfirm"
              label="Password Confirm"
              type="password"
              id="password"
              autoComplete="current-password"
              onChange={(event) => this.setTextHandler(event.target.name, event.target.value)}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              {'Submit'}
            </Button>
            <Grid container>
              <Grid item xs>
                <Link href={Paths.SIGN_IN} variant="body2">
                  {'Login?'}
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
        <Box mt={8}>
          <Copyright />
        </Box>
        <Loading open={this.props.isFetching} />
      </Container> 
    )
  }
}

function mapStateToProps(state) {
  const { resetPassword } = state
  const { isFetching, lastUpdated, data, error } = resetPassword || {
    error: null,
    isFetching: false,
    data: {},
    lastUpdated: null
  }
  return {
    error,
    isFetching,
    data,
    lastUpdated
  }
}

export default withStyles(styles)(connect(mapStateToProps)(ResetPassword)) 