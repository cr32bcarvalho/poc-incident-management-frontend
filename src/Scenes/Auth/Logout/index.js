import React, { Component } from 'react'
import { connect } from 'react-redux'
import Paths from '../../../Router/paths'
import { invalidateUser } from '../../../Commons/Profile/Redux/actions'
import { Redirect } from 'react-router-dom'

class Logout extends Component {
  constructor(props) {
    super(props)
    const { dispatch } = this.props
    dispatch(invalidateUser({}))
  }

  componentDidUpdate(prevProps) {
    
  }
  
  render(){
    const { user } = this.props
    if(!user.id){
      return <Redirect
        to={{
          pathname: Paths.SIGN_IN
        }}
      />
    }
    return (<>{'Saindo'}</>)
  }
}

function mapStateToProps(state) {
  const { profile } = state
  const { isAuthenticating, lastUpdated, user, error } = profile || {
    error: null,
    isAuthenticating: false,
    user: {},
    lastUpdated: null
  }
  return {
    error,
    isAuthenticating,
    user,
    lastUpdated
  }
}

export default connect(mapStateToProps)(Logout) 