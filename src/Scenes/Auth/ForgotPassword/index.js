import React, { Component } from 'react'
import { connect } from 'react-redux'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import TextField from '@material-ui/core/TextField'
import Link from '@material-ui/core/Link'
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'
import Container from '@material-ui/core/Container'
import Paths from '../../../Router/paths'
import Loading from '../../../Commons/Components/Loading'

import {
  fetchDataIfNeeded,
  invalidate
} from './Redux/actions'

import { withStyles } from '@material-ui/core/styles'
import { styles } from '../Commons/styles'
import Copyright from '../../../Commons/Components/Copyright'

class ForgotPassword extends Component {
  constructor(props) {
    super(props)
    this.state = {
      formSubmited: false,
      username: ''
    }

    const { dispatch } = this.props
    dispatch(invalidate({}))
  }

  componentDidUpdate(prevProps) {
    const { data, error, isFetching, dispatch } = this.props
    if (
      this.state.formSubmited &&
      !isFetching &&
      (data !== null || error !== null)
    ) {
      this.setState({ formSubmited: false })
      if (data != null) {
        console.log(`Data: ${data}`)
      }
    } else if (this.state.formSubmited) {
      this.setState({ formSubmited: false }, () => {
        dispatch(invalidate({}))
      })
    }
  }

  setEmailText = (username) => {
    this.setState({
      username
    })
  }

  handleSubmit = (event) => {
    event.preventDefault()
    const { username } = this.state
    const { dispatch } = this.props
    
    const data = {
      username
    }
    dispatch(fetchDataIfNeeded(data))
    this.setState({ formSubmited: true })
  }
  
  render(){
    const { classes } = this.props

    return (
      <Container  component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Typography component="h1" variant="h5">
            {'Forgot password?'}
          </Typography>
          <form className={classes.form} noValidate onSubmit={this.handleSubmit}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              onChange={(event) => this.setEmailText(event.target.value)}
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
            />
           
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              {'Submit'}
            </Button>
            <Grid container>
              <Grid item xs>
                <Link href={Paths.SIGN_IN} variant="body2">
                  {'Login?'}
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
        <Box mt={8}>
          <Copyright />
        </Box>
        <Loading open={this.props.isFetching} />
      </Container> 
    )
  }
}

function mapStateToProps(state) {
  const { forgotPassword } = state
  const { isFetching, lastUpdated, data, error } = forgotPassword || {
    error: null,
    isFetching: false,
    data: {},
    lastUpdated: null
  }
  return {
    error,
    isFetching,
    data,
    lastUpdated
  }
}

export default withStyles(styles)(connect(mapStateToProps)(ForgotPassword)) 