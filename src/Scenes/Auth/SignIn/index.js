import React, { Component } from 'react'
import { connect } from 'react-redux'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import TextField from '@material-ui/core/TextField'
import Link from '@material-ui/core/Link'
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'
import Container from '@material-ui/core/Container'
import { Redirect } from 'react-router-dom'

import {
  authenticateUserIfNeeded,
  invalidateAuthenticatedUser
} from './Redux/actions'
import { withStyles } from '@material-ui/core/styles'
import { styles } from '../Commons/styles'
import Copyright from '../../../Commons/Components/Copyright'
import Loading from '../../../Commons/Components/Loading'
import Paths from '../../../Router/paths'

class SignIn extends Component {
  constructor(props) {
    super(props)
    this.state = {
      formSubmited: false,
      email: '',
      password: ''
    }
    const { dispatch } = this.props
    dispatch(invalidateAuthenticatedUser({}))
  }

  componentDidUpdate(prevProps) {
    const { data, error, isAuthenticating } = this.props
    const { formSubmited } = this.state
    if(formSubmited && !isAuthenticating){
      if(data !=null || error != null){
        this.setState({ formSubmited: false, user: data.user })
      }
    }
  }

 
  setPassword = (password) => {
    this.setState({
      password
    })
  }

  setEmailText = (email) => {
    this.setState({
      email
    })
  }

  handleSubmit = (event) => {
    event.preventDefault()
    const { email, password } = this.state
    const { dispatch } = this.props
    
    const user = {
      email: email,
      password: password
    }
    dispatch(authenticateUserIfNeeded(user))
    this.setState({ formSubmited: true })
  }
  
  render(){
    const { classes, error } = this.props
    const { user } = this.state
    return (
      <>
      {user && (
        <Redirect
          to={{
            pathname: Paths.DASHBOARD,
            state: {}
          }}
        />
      ) || (
        <Container  component="main" maxWidth="xs">
          <CssBaseline />
          <div className={classes.paper}>
            <Typography component="h1" variant="h5">
              {'Sign in'}
            </Typography>
            <form className={classes.form} noValidate onSubmit={this.handleSubmit}>
              {error && (
                <>
                  <CssBaseline />
                  <Typography component="p" align="center" color="error" >
                    {error.message}
                  </Typography>
                </>
              )}
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                onChange={(event) => this.setEmailText(event.target.value)}
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                autoFocus
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={(event) => this.setPassword(event.target.value)}
              />
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                {'Sign in'}
              </Button>
              <Grid container>
                <Grid item xs>
                  <Link href={Paths.FORGOT_PASSWORD} variant="body2">
                    {'Forgot password?'}
                  </Link>
                </Grid>
              </Grid>
            </form>
          </div>
          <Box mt={8}>
            <Copyright />
          </Box>
          <Loading open={this.props.isAuthenticating} />
        </Container>
      )}
      </>
    )
  }
}

function mapStateToProps(state) {
  const { authenticateUser } = state
  const { isAuthenticating, lastUpdated, data, error } = authenticateUser || {
    error: null,
    isAuthenticating: false,
    data: {},
    lastUpdated: null
  }
  return {
    error,
    isAuthenticating,
    data,
    lastUpdated
  }
}

export default withStyles(styles)(connect(mapStateToProps)(SignIn)) 