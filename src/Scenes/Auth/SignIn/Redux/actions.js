import { REQUEST, RECEIVE, INVALIDATE, ERROR } from './constants'
import { signIn } from '../../../../Api/Auth'
import { authError } from '../../../../Api/Commons/constants'
import { mapUserFromToken } from '../../../../Commons/Profile/Interactor'
import { recieveUser } from '../../../../Commons/Profile/Redux/actions'

export const invalidateAuthenticatedUser = (data) => ({
  type: INVALIDATE,
  data
})

const requestLogin = (data) => ({
  type: REQUEST,
  data
})

const authenticateError = (error) => {
  return {
    type: ERROR,
    error
  }
}
const receiveLoginUser = (data) => ({
  type: RECEIVE,
  data,
  receivedAt: Date.now()
})

const receiveLogin = (json) => (dispatch) => {
  const data = json.data
  if (data != null) {
    const user = mapUserFromToken(data.id_token)
    data.user = user

    dispatch(recieveUser({
      user,
      token: {
        id_token: data.id_token, 
        refresh_token: data.refresh_token,
        access_token: data.access_token,
        expires_in: data.expires_in,
        token_type: data.token_type
      }
    }))
    dispatch(receiveLoginUser(data))
  } else {
    return {
      type: ERROR,
      error: {
        status: 400,
        message: json.Message
      },
      receivedAt: Date.now()
    }
  }
}
const authenticateUser = (user) => (dispatch) => {
  dispatch(requestLogin(user))
  signIn(user).then((response) => {
    if(response.error){
      return dispatch(
        authenticateError(response)
      )
    }
    return dispatch(receiveLogin(response))
  })
}

const shouldAuthenticateUser = (state, user) => {
  if (user.email != null && user.password != null) {
    return true
  } else if (user.isAuthenticating) {
    return false
  } else {
    return user.didInvalidate
  }
}

export const authenticateUserIfNeeded = (user) => (dispatch, getState) => {
  if (shouldAuthenticateUser(getState(), user)) {
    return dispatch(authenticateUser(user))
  }
  return dispatch(authenticateError(authError))
}
