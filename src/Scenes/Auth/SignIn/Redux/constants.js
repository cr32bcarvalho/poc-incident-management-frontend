import { ROOT } from '../../../../Store/constants'

const ROOT_SIGN_IN = 'ROOT_SIGN_IN_SCREEN'
export const REQUEST = `${ROOT}/${ROOT_SIGN_IN}/REQUEST`
export const RECEIVE = `${ROOT}/${ROOT_SIGN_IN}/RECEIVE`
export const INVALIDATE = `${ROOT}/${ROOT_SIGN_IN}/INVALIDATE`
export const ERROR = `${ROOT}/${ROOT_SIGN_IN}/ERROR`
