
import { INVALIDATE, REQUEST, RECEIVE, ERROR } from './constants'

const initialState = {
  isAuthenticating: false,
  didInvalidate: false,
  data: null,
  error: null,
  lastUpdated: null
}

const authenticate = (state = initialState, action) => {
  switch (action.type) {
    case INVALIDATE:
      return initialState
    case REQUEST:
      return Object.assign({}, state, {
        isAuthenticating: true,
        didInvalidate: false,
        data: action.user,
        error: null
      })
    case ERROR:
      return Object.assign({}, state, {
        isAuthenticating: false,
        didInvalidate: false,
        error: action.error,
        data: null,
        lastUpdated: action.recievedAt
      })
    case RECEIVE:
      return Object.assign({}, state, {
        isAuthenticating: false,
        didInvalidate: false,
        data: action.data,
        lastUpdated: action.recievedAt
      })
    default:
      return state
  }
}

export const authenticateUser = (state = initialState, action) => {
  switch (action.type) {
    case INVALIDATE:
    case RECEIVE:
    case REQUEST:
    case ERROR:
      return Object.assign({}, state, authenticate(state, action))
    default:
      return state
  }
}
