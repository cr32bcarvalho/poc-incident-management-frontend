import React, { Component } from 'react'
import compose from 'recompose/compose'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import SgqContainer from '../../Commons/SgqContainer'
import Incidents from '../../Commons/IncidentList'
import Paths from '../../../../Router/paths'
import { styles } from './styles'
import {
  fetchDataIfNeeded,
  deleteRow
} from './Redux/actions'

class IncidentScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dataRecieved: false,
    }
    const { dispatch, data } = this.props
    dispatch(fetchDataIfNeeded(data))
  }

  handleDeleteRow = (row) => {
    this.props.dispatch(deleteRow(row))
  }
  // then in any component
  render(){
    const { classes, user, data,isFetching } = this.props
    return(
      <SgqContainer user={user} showLoading={isFetching}>
        <Grid container spacing={3}>
          <Grid item xs={12} md={12} lg={12}>
            <Button variant="contained" color="primary" href={`${Paths.INCIDENTS}create`}>
              {'Cadastrar Incidente'}
            </Button>
          </Grid>
          {data && data.for_analysis && (
            <Grid item xs={12} md={12} lg={12}>
              
              <Paper className={classes.paper}>
                <Incidents user={user} rows={data.for_analysis} title="Aguardando analise"/>
              </Paper>
            </Grid>
          )}
          {data && data.incidents && (
            <Grid item xs={12} md={12} lg={12}>
              <Paper className={classes.paper}>
                <Incidents user={user} rows={data.incidents} title="Lista de Incidentes" handleDeleteRow={(row) => this.handleDeleteRow(row)}/>
              </Paper>
            </Grid>
          )}
        </Grid>
      </SgqContainer>
    )
  }
}

function mapStateToProps(state) {
  const { profile, incidents } = state
  const { list } = incidents
  const { user } = profile || {
    user: {}
  }

  const { lastUpdated, data, error, isFetching } = list || {
    isFetching: false,
    error: null,
    data: {},
    lastUpdated: null
  }
  return {
    isFetching,
    error,
    user,
    data,
    lastUpdated
  }
}

export default compose(
  withStyles(styles, { name: 'Incident' }),
  connect(mapStateToProps),
)(IncidentScreen)
