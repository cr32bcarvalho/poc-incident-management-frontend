import { ROOT } from '../../../../../Store/constants'

const ROOT_LIST_INCIDENTS = 'ROOT_LIST_INCIDENTS'
export const REQUEST = `${ROOT}/${ROOT_LIST_INCIDENTS}/REQUEST`
export const RECEIVE = `${ROOT}/${ROOT_LIST_INCIDENTS}/RECEIVE`
export const INVALIDATE = `${ROOT}/${ROOT_LIST_INCIDENTS}/INVALIDATE`
export const ERROR = `${ROOT}/${ROOT_LIST_INCIDENTS}/ERROR`
