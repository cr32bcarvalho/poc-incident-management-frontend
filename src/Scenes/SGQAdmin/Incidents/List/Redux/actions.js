import { REQUEST, RECEIVE, INVALIDATE, ERROR } from './constants'
import { defaultResponseError } from '../../../../../Api/Commons/constants'
import { fetchIncidents, deleteIncident } from '../../../../../Api/SGQAdmin'

export const invalidate = (data) => ({
  type: INVALIDATE,
  data
})

const request = (data) => ({
  type: REQUEST,
  data
})

const error = (error) => {
  return {
    type: ERROR,
    error
  }
}
const receive = (data) => ({
  type: RECEIVE,
  data: data,
  receivedAt: Date.now()
})

const fetchResponse = (response) => (dispatch) => {
  if (!response.error) {
    dispatch(receive(response))
    return
  } 
  
  return {
    type: ERROR,
    error: {
      status: 400,
      message: response.message
    },
    receivedAt: Date.now()
  }
}

const makeRequest = (token, data) => (dispatch) => {
  dispatch(request(data))
  fetchIncidents(token.id_token).then((response) => {
    if(response.error){
      return dispatch(
        error(response)
      )
    }
    return dispatch(fetchResponse(response))
  })
}

const makeDelete = (token, data) => (dispatch) => {
  deleteIncident(token.id_token, data.uuid).then((response) => {
    if(response && response.error){
      return dispatch(
        error(response)
      )
    }
    dispatch(fetchDataIfNeeded({}))
  })
}
const shouldFetchData = (state, data) => {
  if(!data){
    return true
  }
  if (data && data.fetching) {
    return false
  }
  if (data) {
    return true
  } 
  
  return data.didInvalidate
}


export const syncWithDashboard = (data) => (dispatch) => {
  const list = []
  if(data != null && data.for_analysis!=null){
    list.push(...data.for_analysis)
  }
  if(data != null && data.inicidents!=null){
    list.push(...data.inicidents)
  }
  return dispatch(fetchResponse({incidents: list}))
}

export const fetchDataIfNeeded = (data) => (dispatch, getState) => {
  if (shouldFetchData(getState(), data)) {
    const { profile } = getState()
    return dispatch(makeRequest(profile.token, data))
  }
  return dispatch(error(defaultResponseError))
}

export const deleteRow = (data) => (dispatch, getState) => {
  const { profile } = getState()
  return dispatch(makeDelete(profile.token, data))
}
