import { combineReducers } from 'redux'
import { list } from '../List/Redux/reducers'
import { createOrUpdate } from '../CreateOrUpdate/Redux/reducers'
import { aprove } from '../Aprove/Redux/reducers'

export const incidents = combineReducers({
  list,
  createOrUpdate,
  aprove
})
