import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import SgqContainer from '../../Commons/SgqContainer'
import { styles } from './styles'
import Typography from '@material-ui/core/Typography'
import ListItem from '@material-ui/core/ListItem'
import Divider from '@material-ui/core/Divider'
import ListItemText from '@material-ui/core/ListItemText'
import List from '@material-ui/core/List'

class ShowIncidentScreen extends Component {
  constructor(props) {
    super(props)
    const { data } = props
    const { id } = props.match.params

    const incident = this.findArrayElementById(data.incidents, id)
    this.state = {
      incident: incident && incident || null,
    }
  }

  findArrayElementById(array, id) {
    return array.find((element) => {
      return element.uuid === id
    })
  }
  // then in any component
  render(){

    const { classes, user, history, isFetching } = this.props
    const { incident } = this.state
    return(
      <SgqContainer user={user} showLoading={isFetching}>
        {incident && (
          <Grid item xs={12} md={12} lg={12}>
            <Paper className={classes.paper}>
              <Typography variant="h5" gutterBottom>
                {`${incident.name}`} 
              </Typography>
              <Typography gutterBottom className={classes.title}>
                {incident.description}
              </Typography>
              <List>
                <ListItem alignItems="flex-start">
                  <ListItemText
                    primary="Criado por"
                    secondary={incident.user.name}
                  />
                </ListItem>
                
                {incident && incident.status && (
                  <>
                  <Divider component="li" />
                  <ListItem alignItems="flex-start">
                    <ListItemText
                      primary="Status"
                      secondary={incident.status}
                    />
                  </ListItem>
                  </>
                )}
                {incident && incident.deadline && (
                  <>
                  <Divider component="li" />
                  <ListItem alignItems="flex-start">
                    <ListItemText
                      primary="Data para Correção"
                      secondary={incident.deadline}
                    />
                  </ListItem>
                  </>
                )}
                {incident && incident.is_public && (
                  <>
                  <Divider component="li" />
                  <ListItem alignItems="flex-start">
                    <ListItemText
                      primary="Aberto ao Público"
                      secondary={incident.is_public ? 'sim' : 'não'}
                    />
                  </ListItem>
                  </>
                )}
                {incident && incident.date_created && (
                  <>
                  <Divider component="li" />
                  <ListItem alignItems="flex-start">
                    <ListItemText
                      primary="Criado em"
                      secondary={incident.date_created}
                    />
                  </ListItem>
                  </>
                )}
                {incident && incident.date_modified && (
                  <>
                  <Divider component="li" />
                  <ListItem alignItems="flex-start">
                    <ListItemText
                      primary="Atualizado em"
                      secondary={incident.date_modified}
                    />
                  </ListItem>
                  </>
                )}
              </List>
              <Grid item xs={12}>
                <div className={classes.buttons}>
                  <Button className={classes.button} size="large" variant="contained"  color="secondary" onClick={() => history.goBack()}>
                    {'Voltar'}
                  </Button>
                </div>
              </Grid>
            </Paper>
          </Grid>

        )}
        
      </SgqContainer>
    )
  }
}

function mapStateToProps(state) {
  const { profile, incidents } = state
  const { list } = incidents
  const { user } = profile || {
    user: {}
  }
  const { lastUpdated, data, error, isFetching } = list || {
    isFetching: false,
    error: null,
    data: {},
    lastUpdated: null
  }
  return {
    isFetching,
    error,
    user,
    data,
    lastUpdated
  }
}
  
export default withStyles(styles)(connect(mapStateToProps)(ShowIncidentScreen)) 
