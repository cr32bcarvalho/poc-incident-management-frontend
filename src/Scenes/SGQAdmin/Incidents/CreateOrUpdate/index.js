import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import CssBaseline from '@material-ui/core/CssBaseline'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import SgqContainer from '../../Commons/SgqContainer'
import IncidentForm from '../../Commons/IncidentForm'
import { styles } from './styles'
import {
  invalidate,
  fetchDataIfNeeded
} from './Redux/actions'

class CreateOrUpdateScreen extends Component {
  constructor(props) {
    super(props)
    const { incidents, dispatch } = this.props
    const { id } = props.match.params

    const incident = this.findArrayElementById(incidents, id)
    this.state = {
      formSubmited: false,
      dataRecieved: false,
      incident
    }
    dispatch(invalidate({}))
  }
  
  findArrayElementById(array, id) {
    if(!array){
      return
    }
    return array.find((element) => {
      return element.uuid === id
    })
  }
  componentDidUpdate(prevProps) {
    const { data, error, history,isFetching } = this.props
    const { formSubmited } = this.state
    if(formSubmited){
      if(data !=null || error != null){
        if(isFetching !=null && !isFetching){
          this.setState({ formSubmited: false })
          if(!error){
            history.goBack()
          }
        }
      }
    }
  }
  handleSubmit = (form) => {
    if(form)
      this.setState({ formSubmited: true }, () => this.props.dispatch(fetchDataIfNeeded(form)))
      
  }
  render(){
    const { classes, user, error, history, data, isFetching } = this.props
    const { incident } = this.state
    return(
      <SgqContainer user={user} showLoading={isFetching}>
        <Grid container spacing={3}>
          <Grid item xs={12} md={12} lg={12}>
            <Paper className={classes.paper}>
              {error && (
                  <>
                    <CssBaseline />
                    <Typography component="p" align="center" color="error" >
                      {error.message}
                    </Typography>
                  </>
              )}
              <IncidentForm user={user} history={history} classes={classes} submit={this.handleSubmit} data={incident} title={data && data.uuid ? 'Atualização de Incidente' : 'Cadastro de Incidente' }/>
            </Paper>
          </Grid>
        </Grid>
      </SgqContainer>
    )
  }
}

function mapStateToProps(state) {
  const { profile, incidents } = state
  const { createOrUpdate } = incidents
  const { user } = profile || {
    user: {}
  }
  const { list } = incidents
  const { lastUpdated, data, error, isFetching } = createOrUpdate || {
    isFetching: false,
    error: null,
    data: {},
    lastUpdated: null
  }
  return {
    isFetching,
    error,
    user,
    data,
    incidents: list.data.incidents,
    lastUpdated
  }
}
  
export default withStyles(styles)(connect(mapStateToProps)(CreateOrUpdateScreen)) 
