import { REQUEST, RECEIVE, INVALIDATE, ERROR } from './constants'
import { defaultResponseError } from '../../../../../Api/Commons/constants'
import { createOrUpdateIncident } from '../../../../../Api/SGQAdmin'

export const invalidate = (data) => ({
  type: INVALIDATE,
  data
})

const request = (data) => ({
  type: REQUEST,
  data
})

const error = (error) => {
  return {
    type: ERROR,
    error
  }
}
const receive = (data) => ({
  type: RECEIVE,
  data: data,
  receivedAt: Date.now()
})

const fetchResponse = (response) => (dispatch) => {
  if (!response.error) {
    dispatch(receive(response))
    return
  } 
  
  return {
    type: ERROR,
    error: {
      status: 400,
      message: response.message
    },
    receivedAt: Date.now()
  }
}

const makeRequest = (token, data) => (dispatch) => {
  dispatch(request(data))
  createOrUpdateIncident(token.id_token, data).then((response) => {
    if(response.error){
      return dispatch(
        error(response)
      )
    }
    return dispatch(fetchResponse(response))
  })
}

const shouldFetchData = (state, data) => {
  if(!data){
    return true
  }
  if (data && data.fetching) {
    return false
  }
  if (data) {
    return true
  } 
  
  return data.didInvalidate
}

export const fetchDataIfNeeded = (data) => (dispatch, getState) => {
  if (shouldFetchData(getState(), data)) {
    const { profile } = getState()
    return dispatch(makeRequest(profile.token, data))
  }
  return dispatch(error(defaultResponseError))
}
