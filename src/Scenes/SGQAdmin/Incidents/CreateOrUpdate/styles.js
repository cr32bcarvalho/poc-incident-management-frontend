import { useTheme } from '@material-ui/core/styles'


export const styles = () => {
  const theme = useTheme()
  return {
    paper: {
      padding: theme.spacing(2),
      display: 'flex',
      overflow: 'auto',
      flexDirection: 'column',
    },
    buttons: {
      display: 'flex',
      justifyContent: 'space-between',
    },
    button: {
      marginTop: theme.spacing(3),
      marginLeft: theme.spacing(1),
    }
  }
}
