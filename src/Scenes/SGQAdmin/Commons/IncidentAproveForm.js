import React from 'react'
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers'
import TextField from '@material-ui/core/TextField'
import DateFnsUtils from '@date-io/date-fns'
import Select from '@material-ui/core/Select'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import { fetchAssociations, fetchRequirements, fetchStandards } from '../../../Api/Compliance/StandardsAndRegulations'

function IncidentAproveForm(params) {
  const { user, classes, title, submit, history, data } = params
  const [associations, setAssociations] = React.useState()
  const [standards, setStandards] = React.useState()
  const [requirements, setRequirements] = React.useState()
  const [selectedDate, setSelectedDate] = React.useState(data && data.deadline || null)
  const [state, setState] = React.useState({
    errors: {
      name: false,
      description: false
    },
    uuid: data && data.uuid || null,
    name: data && data.name || '',
    description: data && data.description || '',
    association_id: data && data.association_id || '',
    standard_id: data && data.standard_id || '',
    requirement_id: data && data.requirement_id || '',
    status: data && data.status || 'analysis',
    is_public: data && data.is_public || false
  })

  React.useEffect(() => {
    // Update the document title using the browser API
    fetchAssociations().then((response) => {
      if(response && response.associations)
        setAssociations(response.associations)      
    })
    if(state.association_id)
      getStandards(state.association_id)
    if(state.standard_id)
      getRequirements(state.standard_id) 
  }, [])

  const getStandards = (associationId) => {
    fetchStandards(associationId).then((response) => {
      if(response && response.standards)
        setStandards(response.standards)      
    })
  }

  const getRequirements = (standardId) => {
    fetchRequirements(standardId).then((response) => {
      if(response && response.requirements)
        setRequirements(response.requirements)      
    })
  }
  
  const handleDateChange = (date) => {
    setSelectedDate(date)
  }

  const handleChange = (event) => {
    if(event.target.name=='association_id'){
      setState({...state, standards: []})
      setState({...state, requirements: []})
      
      getStandards(event.target.value)
    }
    if(event.target.name=='standard_id'){
      setState({...state, requirements: []})
      getRequirements(event.target.value)
    }
    setState(
      {...state, 
        [event.target.name]:event.target.value,
        errors: { ...state.errors, [event.target.name]: false }
      }
    )
    
  }

  const handleSubmit = () => {
    const errors = {
      name: (!state.name),
      description: (!state.description)
    }
    if(errors.name || errors.description){
      setState({ ...state, errors })
      return
    }

    let form = {...state}
    if(selectedDate){
      form = {
        ...form,
        deadline: selectedDate
      }
    }

    submit(form)
  }
  
  return (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <Typography component="h1" variant="h4" color="inherit" noWrap className={classes.title}>
          {title}
        </Typography>
      </Grid>
      <Grid item xs={12} md={8}>
        <Typography component="h5" variant="h6" color="textSecondary" noWrap className={classes.title}>
          Título
        </Typography>
        <Typography component="h5" variant="h5" color="textPrimary" noWrap className={classes.title}>
          {state && state.name && state.name}
        </Typography>
      </Grid>
      <Grid item xs={12} md={8}>
        <Typography component="h5" variant="h6" color="textSecondary" noWrap className={classes.title}>
          Descrição
        </Typography>
        <Typography component="h5" variant="h5" color="textPrimary" noWrap className={classes.title}>
          {state && state.description && state.description}
        </Typography>
      </Grid>
      {user && user.department == 'engineers' && (
          <>
            <Grid item xs={12} md={8}>
              <FormControl
                fullWidth>
                <InputLabel id="demo-simple-select-label">Organização </InputLabel>
                <Select
                  name="association_id"
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={state && state.association_id && state.association_id}
                  onChange={handleChange}
                  IconComponent={() => (<ArrowDropDownIcon />)}
                > 
                  { associations && (
                    associations.map((association) => (
                      <MenuItem key={association.uuid} value={association.uuid}>{association.name}</MenuItem>
                    ))
                  )}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} md={8}>
              <FormControl
                fullWidth>
                <InputLabel id="demo-simple-select-label">Normas </InputLabel>
                <Select
                  name="standard_id"
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={state && state.standard_id && state.standard_id}
                  onChange={handleChange}
                  IconComponent={() => (<ArrowDropDownIcon />)}
                > 
                  { standards && (
                    standards.map((standard) => (
                      <MenuItem key={standard.uuid} value={standard.uuid}>{standard.title}</MenuItem>
                    ))
                  )}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} md={8}>
              <FormControl
                fullWidth>
                <InputLabel id="demo-simple-select-label">Regulamento</InputLabel>
                <Select
                  name="requirement_id"
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={state && state.requirement_id && state.requirement_id}
                  onChange={handleChange}
                  IconComponent={() => (<ArrowDropDownIcon />)}
                > 
                  { requirements && (
                    requirements.map((requirement) => (
                      <MenuItem key={requirement.uuid} value={requirement.uuid}>{requirement.title}</MenuItem>
                    ))
                  )}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <Grid item xs={12} md={4}>
                <FormControl
                  fullWidth>
                  <InputLabel id="demo-simple-select-label">Disponibilizar no FEED </InputLabel>
                  <Select
                    name="is_public"
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={state && state.is_public && state.is_public}
                    onChange={handleChange}
                    IconComponent={() => (<ArrowDropDownIcon />)}
                  >
                    <MenuItem value={true}>Sim</MenuItem>
                    <MenuItem value={false}>Não</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
            <Grid item xs={12}>
              <Grid item xs={12} md={4}>
                <FormControl
                  fullWidth>
                  <InputLabel id="demo-simple-select-label">Status </InputLabel>
                  <Select
                    name="status"
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={state && state.status && state.status}
                    onChange={handleChange}
                    IconComponent={() => (<ArrowDropDownIcon />)}
                  >
                    <MenuItem value={'analysis'}>Em analise</MenuItem>
                    <MenuItem value={'approved'}>Aprovado</MenuItem>
                    <MenuItem value={'solved'}>Resolvido</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
            <Grid item xs={12}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                  xs={12}
                  disableToolbar
                  variant="inline"
                  format="dd/MM/yyyy"
                  margin="normal"
                  id="date-picker-inline"
                  label="Data para correção"
                  value={selectedDate}
                  onChange={handleDateChange}
                  KeyboardButtonProps={{
                    'aria-label': 'change date',
                  }}
                />
              </MuiPickersUtilsProvider>
            </Grid>
          </>
      )}
      <Grid item xs={12}>
        <div className={classes.buttons}>
          <Button className={classes.button} size="large" variant="contained"  color="secondary" onClick={() => history.goBack()}>
            {'Voltar'}
          </Button>
          <Button className={classes.button} size="large" variant="contained"  color="primary" onClick={handleSubmit}>
            {'Enviar'}
          </Button>
        </div>
      </Grid>
    </Grid>
  )
}

export default IncidentAproveForm 
