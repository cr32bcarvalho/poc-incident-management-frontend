import React from 'react'
import clsx from 'clsx'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Link from '@material-ui/core/Link'
import MenuIcon from '@material-ui/icons/Menu'
import AccountCircle from '@material-ui/icons/AccountCircle'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'
import Hidden from '@material-ui/core/Hidden'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import Paths from '../../../Router/paths'

export default function SgqAppBar(params) {
  const { title, open, classes, handleDrawerToggle, handleShiftToggle, user } = params
  const [anchorEl, setAnchorEl] = React.useState(null)
  const openProfileMenu = Boolean(anchorEl)
  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }
  
  return ( 
    <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
      <Toolbar className={classes.toolbar}>
        <Hidden smUp implementation="css">
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={() => handleDrawerToggle()}
            className={clsx(classes.menuButton)}
          >
            <MenuIcon />
          </IconButton>
        </Hidden>
        <Hidden xsDown implementation="css">
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={() => handleShiftToggle()}
            className={clsx(classes.menuButtonShift, open && classes.menuButtonShiftHidden)}
          >
            <MenuIcon />
          </IconButton>
        </Hidden>
        <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
          {title}
        </Typography>
        <div>
          { user && (
            <Hidden xsDown  >
              {`Bem vindo ${user.name}`}
            </Hidden>
          )}
          <IconButton
            aria-label="account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            onClick={handleMenu}
            color="inherit"
          >
            <AccountCircle />
          </IconButton>
          <Menu
            id="menu-appbar"
            anchorEl={anchorEl}
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
            keepMounted
            transformOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
            open={openProfileMenu}
            onClose={handleClose}
          >
            <MenuItem component={Link} href={`${Paths.LOGOUT}`} >
              {'Sair'}
            </MenuItem>
          </Menu>
        </div>
      </Toolbar>
    </AppBar>  
  )
}