import React from 'react'
import Link from '@material-ui/core/Link'
import { parseISO } from 'date-fns';
import { format } from 'date-fns-tz'
import Typography from '@material-ui/core/Typography'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import DeleteForeverIcon from '@material-ui/icons/DeleteForever'
import EditIcon from '@material-ui/icons/Edit'
import VisibilityIcon from '@material-ui/icons/Visibility'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Paths from '../../../Router/paths'
// Generate Order Data

function preventDefault(event) {
  event.preventDefault()
}

export default function IncidentList(params) {

  const [open, setOpen] = React.useState(false)
  const [selected, setSelected] = React.useState(null)
  const { title, rows, seeMore, user, handleDeleteRow } = params

  const handleClickOpen = (event, row) => {
    preventDefault(event)
    setSelected(row)
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const handleConfirm = () => {
    if(handleDeleteRow && selected){
      handleDeleteRow(selected)
      setOpen(false)
    }
  }
  return (
    <React.Fragment>
      <Typography component="h2" variant="h6" color="primary" gutterBottom>
        {title && title}
      </Typography>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>Nome</TableCell>
            <TableCell>Descrição</TableCell>
            <TableCell>Status</TableCell>
            <TableCell>Criada em</TableCell>
            <TableCell>Alterada em</TableCell>
            <TableCell>Criada por</TableCell>
            <TableCell align="right"></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.uuid}>
              <TableCell>
                <Link color="primary" href={`${Paths.INCIDENTS}${row.uuid}/show`}>
                  {row.name}
                </Link>
              </TableCell>
              <TableCell>{row.description}</TableCell>
              <TableCell>{row.status}</TableCell>
              <TableCell>
                {format(parseISO(row.date_created), 'dd/MM/yyyy', {
                  timeZone: 'America/Sao_Paulo',
                })}
              </TableCell>
              <TableCell>
                {format(parseISO(row.date_modified), 'dd/MM/yyyy', {
                  timeZone: 'America/Sao_Paulo',
                })}
              </TableCell>
              <TableCell>{row.user.name}</TableCell>
              <TableCell align="right">
                
                {user && row.user.public_id === user.id && row.status === 'analysis' && (
                  <Link color="primary" href={`${Paths.INCIDENTS}${row.uuid}/update`}>
                    <EditIcon fontSize="small" />
                  </Link>
                )}
                {user && user.department === 'engineers' && (row.status === 'analysis' || row.status === 'approved')  && (
                  <Link color="secondary" href={`${Paths.INCIDENTS}${row.uuid}/aprove`}>
                    <CheckCircleIcon fontSize="small" />
                  </Link>
                )}
                {user && row.user.public_id === user.id && row.status == 'analysis' && (
                  <Link color="error" onClick={ (e) => handleClickOpen(e, row) } href="#">
                    <DeleteForeverIcon fontSize="small" />
                  </Link>
                )}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      {seeMore && (
        <div>
          <Link color="primary" href="" onClick={preventDefault}>
            See more orders
          </Link>
        </div>
      )}
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Esta ação não poderá ser desfeita."}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {`Tem certeza que deseja apagar o ${selected && selected.name}, esta ação não poderá ser desfeita.`}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancelar
          </Button>
          <Button onClick={handleConfirm} color="primary" autoFocus>
            Confirmar
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  )
}