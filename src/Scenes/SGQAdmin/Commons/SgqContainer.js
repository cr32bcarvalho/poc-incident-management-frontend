import React from 'react'
import CssBaseline from '@material-ui/core/CssBaseline'
import Container from '@material-ui/core/Container'
import { makeStyles } from '@material-ui/core/styles'
import SgqAppBar from './SgqAppBar'
import DrawerMenu from './DrawerMenu'
import { styles } from './styles'
import Loading from '../../../Commons/Components/Loading'
import Copyright from '../../../Commons/Components/Copyright'
import Box from '@material-ui/core/Box'

const useStyles = makeStyles((theme) => styles(theme))

function SgqContainer(params) {
  const classes = useStyles()
  const { user, children, showLoading } = params
  const [mobileOpen, setMobileOpen] = React.useState(false)
  const [open, setOpen] = React.useState(false)

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen)
  }

  const handleShiftToggle = () => {
    setOpen(!open)
  }

  return (
    <div className={classes.root}>
      <CssBaseline />
      <SgqAppBar 
        title="POC SGQ - PUC"
        classes={classes} 
        open={open}
        user={user}
        handleShiftToggle={handleShiftToggle}
        handleDrawerToggle={handleDrawerToggle} />
      <DrawerMenu 
        classes={classes} 
        open={open}
        mobileOpen={mobileOpen}
        handleShiftToggle={handleShiftToggle}
        handleDrawerToggle={handleDrawerToggle} />
        
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="lg" className={classes.container}>
          {children}
          <Box mt={8}>
            <Copyright />
          </Box>
        </Container>    
           
      </main>
      <Loading open={showLoading ? showLoading : false} />
    </div>
  )
}

export default SgqContainer 
