import React from 'react'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import DashboardIcon from '@material-ui/icons/Dashboard'
import PeopleIcon from '@material-ui/icons/People'
import LayersIcon from '@material-ui/icons/Layers'
import Link from '@material-ui/core/Link'
import Paths from '../../../Router/paths'

function ListItemLink(props) {
  return <ListItem button component="a" {...props} />
}

export const navigation = () => (
  <div>
    <ListItemLink button href={`${Paths.DASHBOARD}`}>
      <ListItemIcon>
        <DashboardIcon />
      </ListItemIcon>
      <ListItemText primary="Dashboard" />
    </ListItemLink>
    <ListItemLink button href={`${Paths.INCIDENTS}`}>
      <ListItemIcon>
        <LayersIcon />
      </ListItemIcon>
      <ListItemText primary="Incidentes"  />
    </ListItemLink>
    <ListItemLink button href={`${Paths.CONSULTANCIES}`}>
      <ListItemIcon>
        <PeopleIcon />
      </ListItemIcon>
      <ListItemText primary="Consultorias" />
    </ListItemLink>
  </div>
)