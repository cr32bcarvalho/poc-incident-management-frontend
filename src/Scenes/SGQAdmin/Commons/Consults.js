import React from 'react'
import Link from '@material-ui/core/Link'
import { parseISO } from 'date-fns';
import { format } from 'date-fns-tz'
import Typography from '@material-ui/core/Typography'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paths from '../../../Router/paths'
// Generate Order Data

function preventDefault(event) {
  event.preventDefault()
}

export default function Consults(params) {

  const { title, rows } = params

  return (
    <React.Fragment>
      <Typography component="h2" variant="h6" color="primary" gutterBottom>
        {title && title}
      </Typography>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>Nome</TableCell>
            <TableCell>Documento</TableCell>
            <TableCell>Status</TableCell>
            <TableCell>Criada em</TableCell>
            <TableCell>Alterada em</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.uuid}>
              <TableCell>
                <Link color="primary" href={`${Paths.CONSULTANCIES}${row.id}/show`}>
                  {row.name}
                </Link>
              </TableCell>
              <TableCell>{row.nif}</TableCell>
              <TableCell>{row.status}</TableCell>
              <TableCell>
                {format(parseISO(row.date_created), 'dd/MM/yyyy', {
                  timeZone: 'America/Sao_Paulo',
                })}
              </TableCell>
              <TableCell>
                {format(parseISO(row.date_modified), 'dd/MM/yyyy', {
                  timeZone: 'America/Sao_Paulo',
                })}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </React.Fragment>
  )
}