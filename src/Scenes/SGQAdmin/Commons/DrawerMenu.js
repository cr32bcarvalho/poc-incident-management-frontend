import React from 'react'
import clsx from 'clsx'
import Hidden from '@material-ui/core/Hidden'
import List from '@material-ui/core/List'
import Divider from '@material-ui/core/Divider'
import Drawer from '@material-ui/core/Drawer'
import IconButton from '@material-ui/core/IconButton'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import { navigation } from './navigation'


export default function DrawerMenu(params) {
  const { open, mobileOpen, classes, handleDrawerToggle, handleShiftToggle, history } = params

  return (
    <nav className={classes.drawer} aria-label="mailbox folders">
      {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
      <Hidden smUp implementation="css">
        <Drawer
          variant="temporary"
          classes={{
            paper: classes.drawerPaper,
          }}
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          <div className={classes.appBarSpacer} />
          <Divider />
          <List>{navigation(history)}</List>
        </Drawer>
      </Hidden>
      <Hidden xsDown implementation="css">
        <Drawer
          classes={{
            paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
          }}
          variant="permanent"
          open
        >
          <div className={classes.toolbarIcon}>
            <IconButton onClick={handleShiftToggle}>
              <ChevronLeftIcon />
            </IconButton>
          </div>              
          <Divider />
          <List>{navigation(history)}</List>
        </Drawer>
      </Hidden>
    </nav>
  )
}