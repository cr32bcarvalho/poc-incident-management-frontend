import { combineReducers } from 'redux'
import { consultancies } from '../ConsultanciesAndAdvisory/List/Redux/reducers'

export const consultanciesAndAdvisory = combineReducers({
  consultancies
})
