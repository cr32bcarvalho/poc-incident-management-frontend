import { REQUEST, RECEIVE, INVALIDATE, ERROR } from './constants'
import { defaultResponseError } from '../../../../../../Api/Commons/constants'
import { fetchConsults } from '../../../../../../Api/Compliance'

export const invalidate = (data) => ({
  type: INVALIDATE,
  data
})

const request = (data) => ({
  type: REQUEST,
  data
})

const error = (error) => {
  return {
    type: ERROR,
    error
  }
}
const receive = (data) => ({
  type: RECEIVE,
  data: data,
  receivedAt: Date.now()
})

const fetchResponse = (response) => (dispatch) => {
  if (!response.error) {
    dispatch(receive(response))
    return
  } 
  
  return {
    type: ERROR,
    error: {
      status: 400,
      message: response.message
    },
    receivedAt: Date.now()
  }
}

const makeRequest = (data) => (dispatch) => {
  dispatch(request(data))
  fetchConsults().then((response) => {
    if(response.error){
      return dispatch(
        error(response)
      )
    }
    return dispatch(fetchResponse(response))
  })
}

const shouldFetchData = (data) => {
  if(!data){
    return true
  }
  if (data && data.fetching) {
    return false
  }
  if (data) {
    return true
  } 
  
  return data.didInvalidate
}

export const fetchDataIfNeeded = (data) => (dispatch) => {
  if (shouldFetchData(data)) {
    return dispatch(makeRequest(data))
  }
  return dispatch(error(defaultResponseError))
}
