import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import SgqContainer from '../../../Commons/SgqContainer'
import Consults from '../../../Commons/Consults'
import { styles } from './styles'
import {
  fetchDataIfNeeded
} from './Redux/actions'

class ConsultanciesList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dataRecieved: false,
    }
    const { dispatch, data } = this.props
    dispatch(fetchDataIfNeeded(data))
  }

  
  render(){
    const { classes, user, data, isFetching } = this.props
    return(
      <SgqContainer user={user} showLoading={isFetching}>
        <Grid container spacing={3}>
          {data && data.consulting && (
            <Grid item xs={12} md={12} lg={12}>
                
              <Paper className={classes.paper}>
                <Consults rows={data.consulting} title="Consultorias"/>
              </Paper>
            </Grid>
          )}
        </Grid>
      </SgqContainer>
    )
  }
}

function mapStateToProps(state) {
  const { profile, consultanciesAndAdvisory } = state
  const { consultancies } = consultanciesAndAdvisory
  const { user } = profile || {
    user: {}
  }
  const { lastUpdated, data, error, isFetching } = consultancies|| {
    isFetching: false,
    error: null,
    data: {},
    lastUpdated: null
  }
  return {
    isFetching,
    error,
    user,
    data,
    lastUpdated
  }
}
  
export default withStyles(styles)(connect(mapStateToProps)(ConsultanciesList)) 
