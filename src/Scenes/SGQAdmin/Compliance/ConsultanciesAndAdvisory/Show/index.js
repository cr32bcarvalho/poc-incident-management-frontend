import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import SgqContainer from '../../../Commons/SgqContainer'
import { styles } from './styles'
import ListItem from '@material-ui/core/ListItem'
import Divider from '@material-ui/core/Divider'
import ListItemText from '@material-ui/core/ListItemText'
import List from '@material-ui/core/List'


class ShowConsultScreen extends Component {
  constructor(props) {
    super(props)
    const { data } = props
    const { id } = props.match.params

    const consult = this.findArrayElementById(data.consulting, id)
    this.state = {
      consult: consult && consult || null,
    }
  }

  findArrayElementById(array, id) {
    return array.find((element) => {
      return element.id === id
    })
  }
  // then in any component
  render(){

    const { classes, user, history, isFetching } = this.props
    const { consult } = this.state
    return(
      <SgqContainer user={user} showLoading={isFetching}>
        {consult && (
          <Grid item xs={12} md={12} lg={12}>
            <Paper className={classes.paper}>
              <List>
                <ListItem alignItems="flex-start">
                  <ListItemText
                    primary="Nome"
                    secondary={consult.name}
                  />
                </ListItem>
                <Divider component="li" />
                <ListItem alignItems="flex-start">
                  <ListItemText
                    primary="Documento"
                    secondary={consult.nif}
                  />
                </ListItem>
                <Divider component="li" />
                <ListItem alignItems="flex-start">
                  <ListItemText
                    primary="Status"
                    secondary={consult.status}
                  />
                </ListItem>
                <Divider component="li" />
                <ListItem alignItems="flex-start">
                  <ListItemText
                    primary="Criado em"
                    secondary={consult.date_created}
                  />
                </ListItem>
                <Divider component="li" />
                <ListItem alignItems="flex-start">
                  <ListItemText
                    primary="Atualizado em"
                    secondary={consult.date_modified}
                  />
                </ListItem>
              </List>
              <Grid item xs={12}>
                <div className={classes.buttons}>
                  <Button className={classes.button} size="large" variant="contained"  color="secondary" onClick={() => history.goBack()}>
                    {'Voltar'}
                  </Button>
                </div>
              </Grid>
            </Paper>
          </Grid>

        )}
        
      </SgqContainer>
    )
  }
}

function mapStateToProps(state) {
  const { profile, consultanciesAndAdvisory } = state
  const { consultancies } = consultanciesAndAdvisory
  const { user } = profile || {
    user: {}
  }
  const { lastUpdated, data, error, isFetching } = consultancies || {
    isFetching: isFetching,
    error: null,
    data: {},
    lastUpdated: null
  }
  return {
    isFetching,
    error,
    user,
    data,
    lastUpdated
  }
}
  
export default withStyles(styles)(connect(mapStateToProps)(ShowConsultScreen)) 
