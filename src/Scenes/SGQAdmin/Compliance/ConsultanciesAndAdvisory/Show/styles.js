import { useTheme } from '@material-ui/core/styles'


export const styles = () => {
  const theme = useTheme()
  return {
    paper: {
      padding: theme.spacing(2),
      display: 'flex',
      overflow: 'auto',
      flexDirection: 'column',
    }
  }
}
