import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import SgqContainer from '../Commons/SgqContainer'
import IncidentList from '../Commons/IncidentList'
import { styles } from './ConsultanciesAndAdvisory/List/styles'
import {
  invalidate,
  fetchDataIfNeeded
} from './Redux/actions'

class Dashboard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dataRecieved: false,
    }
    const { dispatch, data } = this.props
    dispatch(invalidate({}))
    dispatch(fetchDataIfNeeded(data))
  }

  
  render(){
    const { classes, user, error, data } = this.props

    return(
      <SgqContainer user={user}>
        <Grid container spacing={3}>
          {data && data.for_analysis && (
            <Grid item xs={12}>
              <Paper className={classes.paper}>
                <IncidentList user={user} rows={data.for_analysis} title="Aguardando analise"/>
              </Paper>
            </Grid>
          )}
          {data && data.incidents && (
            <Grid item xs={12} md={12} lg={12}>
              <Paper className={classes.paper}>
                <IncidentList user={user} rows={data.incidents} title="Meus Incidentes"/>
              </Paper>
            </Grid>
          )}
        </Grid>
      </SgqContainer>
    )
  }
}

function mapStateToProps(state) {
  const { profile, dashboard } = state
  const { user } = profile || {
    user: {}
  }
  const { lastUpdated, data, error } = dashboard || {
    error: null,
    data: {},
    lastUpdated: null
  }
  return {
    error,
    user,
    data,
    lastUpdated
  }
}
  
export default withStyles(styles)(connect(mapStateToProps)(Dashboard)) 
