import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import SgqContainer from '../Commons/SgqContainer'
import IncidentList from '../Commons/IncidentList'
import { styles } from './styles'
import {
  invalidate,
  fetchDataIfNeeded,
  deleteRow
} from './Redux/actions'

class Dashboard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dataRecieved: false,
    }
    const { dispatch, data } = this.props
    dispatch(fetchDataIfNeeded(data))
  }

  handleDeleteRow = (row) => {
    this.props.dispatch(deleteRow(row))
  }
  
  render(){
    const { classes, user, data, isFetching } = this.props

    return(
      <SgqContainer user={user} showLoading={isFetching}>
        <Grid container spacing={3}>
          {data && data.for_analysis && (
            <Grid item xs={12}>
              <Paper className={classes.paper}>
                <IncidentList user={user} rows={data.for_analysis} title="Aguardando analise"  handleDeleteRow={(row) => this.handleDeleteRow(row)}/>
              </Paper>
            </Grid>
          )}
          {data && data.incidents && (
            <Grid item xs={12} md={12} lg={12}>
              <Paper className={classes.paper}>
                <IncidentList user={user} rows={data.incidents} title="Meus Incidentes"  handleDeleteRow={(row) => this.handleDeleteRow(row)}/>
              </Paper>
            </Grid>
          )}
        </Grid>
      </SgqContainer>
    )
  }
}

function mapStateToProps(state) {
  const { profile, dashboard } = state
  const { user } = profile || {
    user: {}
  }
  const { lastUpdated, data, error, isFetching } = dashboard || {
    isFetching: false, 
    error: null,
    data: {},
    lastUpdated: null
  }
  return {
    isFetching,
    error,
    user,
    data,
    lastUpdated
  }
}
  
export default withStyles(styles)(connect(mapStateToProps)(Dashboard)) 
