import { REQUEST, RECEIVE, INVALIDATE, ERROR } from './constants'
import { defaultResponseError } from '../../../../Api/Commons/constants'
import { fetchDashboard, deleteIncident } from '../../../../Api/SGQAdmin'
import { syncWithDashboard } from '../../Incidents/List/Redux/actions'

export const invalidate = (data) => ({
  type: INVALIDATE,
  data
})

const request = (data) => ({
  type: REQUEST,
  data
})

const error = (error) => {
  return {
    type: ERROR,
    error
  }
}
const receive = (data) => ({
  type: RECEIVE,
  data: data,
  receivedAt: Date.now()
})

const fetchResponse = (response) => (dispatch) => {
  if (!response.error) {
    dispatch(receive(response))
    dispatch(syncWithDashboard(response))
    return
  } 
  
  return {
    type: ERROR,
    error: {
      status: 400,
      message: response.message
    },
    receivedAt: Date.now()
  }
}

const makeRequest = (token, data) => (dispatch) => {
  dispatch(request(data))
  fetchDashboard(token.id_token).then((response) => {
    if(response.error){
      return dispatch(
        error(response)
      )
    }
    return dispatch(fetchResponse(response))
  })
}

const makeDelete = (token, data) => (dispatch) => {
  deleteIncident(token.id_token, data.uuid).then((response) => {
    if(response && response.error){
      return dispatch(
        error(response)
      )
    }
    dispatch(fetchDataIfNeeded({}))
  })
}

const shouldFetchData = (state, data) => {
  if(!data){
    return true
  }
  if (data && data.fetching) {
    return false
  }
  if (data) {
    return true
  } 
  
  return data.didInvalidate
}

export const fetchDataIfNeeded = (data) => (dispatch, getState) => {
  if (shouldFetchData(getState(), data)) {
    const { profile } = getState()
    return dispatch(makeRequest(profile.token, data))
  }
  return dispatch(error(defaultResponseError))
}

export const deleteRow = (data) => (dispatch, getState) => {
  const { profile } = getState()
  return dispatch(makeDelete(profile.token, data))
}