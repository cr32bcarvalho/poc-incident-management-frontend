import { ROOT } from '../../../../Store/constants'

const ROOT_DASHBOARD = 'ROOT_DASHBOARD'
export const REQUEST = `${ROOT}/${ROOT_DASHBOARD}/REQUEST`
export const RECEIVE = `${ROOT}/${ROOT_DASHBOARD}/RECEIVE`
export const INVALIDATE = `${ROOT}/${ROOT_DASHBOARD}/INVALIDATE`
export const ERROR = `${ROOT}/${ROOT_DASHBOARD}/ERROR`
