export const mapUser = (userApi) => {
  const user = {
    profilePicture: userApi.Foto != null ? userApi.Foto : null,
    id: userApi.Id != null ? userApi.Id : null,
    name: userApi.Nome != null ? userApi.Nome : '',
    email: userApi.Email != null ? userApi.Email : '',
    cpf: userApi.Cpf != null ? userApi.Cpf : '',
    birthDate: userApi.DataNascimento != null ? userApi.DataNascimento : '',
    telephone: userApi.Telefone != null ? userApi.Telefone : '',
  }
  return user
}

export const mapUserFromToken = (token) => {
  const userResp = getUserFromToken(token)
  return {
    id: userResp.sub,
    name: userResp.name,
    department: userResp['custom:department'], 
    phone_number: userResp.phone_number,
    email: userResp.email,
    email_verified: userResp.email_verified
       
  }
}

export const getUserFromToken = (token) => {
  if (token) {
    try {
      return JSON.parse(atob(token.split('.')[1]))
    } catch (error) {
      // ignore
    }
  }
  return null
}