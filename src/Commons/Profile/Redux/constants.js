import { ROOT } from '../../../Store/constants'

const ROOT_PROFILE = 'ROOT_PROFILE'
export const REQUEST = `${ROOT}/${ROOT_PROFILE}/REQUEST`
export const RECEIVE = `${ROOT}/${ROOT_PROFILE}/RECEIVE`
export const INVALIDATE = `${ROOT}/${ROOT_PROFILE}/INVALIDATE`
export const ERROR = `${ROOT}/${ROOT_PROFILE}/ERROR`
