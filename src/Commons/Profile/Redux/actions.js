import storage from 'redux-persist/lib/storage/session' // defaults to localStorage for web
import { REQUEST, RECEIVE, INVALIDATE, ERROR } from './constants'
import {
  USER,
  TOKEN
} from '../../../Commons/Constants/Async'

export const invalidateUser = () => ({
  type: INVALIDATE
})

export const requestUser = () => ({
  type: REQUEST
})

export const recieveUser = (data) => ({
  type: RECEIVE,
  data
})

export const error = (message) => ({
  type: ERROR,
  message
})

export const getProfile = () => (dispatch) => {
  const user = storage.getItem(USER).toJson()
  if(!user){
    return dispatch(requestUser())
  }
  
  return user
}
export const getToken = () => (dispatch) => {
  const token = storage.getItem(TOKEN).toJson()
  if (!token) {
    return dispatch(requestUser())
  }
  return token
}