import storage from 'redux-persist/lib/storage/session' // defaults to localStorage for web
import { INVALIDATE, REQUEST, RECEIVE, ERROR } from './constants'

import {
  USER,
  TOKEN
} from '../../Constants/Async'

const initialState = {
  isFetching: false,
  didInvalidate: false,
  user: {
    id: null,
    name: '',
    email: null,
    department: null
  },
  token: null, 
  error: null
}

const storeUserData = async (user, token = null) => {
  try {
    if (token) {
      storage.setItem(TOKEN, JSON.stringify(token))
    }
    if (user) {
      storage.setItem(USER, JSON.stringify(user))
    }
  } catch (error) {
    console.log(error)
    // Error saving data
  }
}

const profileUser = (state = initialState, action) => {
  switch (action.type) {
    case INVALIDATE:
      return initialState
    case REQUEST:
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        error: null
      })
    case ERROR:
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        error: action.error,
        lastUpdated: action.recievedAt
      })
    case RECEIVE: {
      const { token, user } = action.data
      storeUserData(user, token)
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        user: user,
        token: token,
        lastUpdated: action.recievedAt
      })
    }
    default:
      return state
  }
}

export const profile = (state = initialState, action) => {
  switch (action.type) {
    case INVALIDATE:
    case RECEIVE:
    case REQUEST:
    case ERROR:
      return Object.assign({}, state, profileUser(state, action))
    default:
      return state
  }
}
