import { combineReducers } from 'redux'
import { profile } from '../Commons/Profile/Redux/reducers'
import { authenticateUser } from '../Scenes/Auth/SignIn/Redux/reducers'
import { forgotPassword } from '../Scenes/Auth/ForgotPassword/Redux/reducers'
import { resetPassword } from '../Scenes/Auth/ResetPassword/Redux/reducers'
import { dashboard } from '../Scenes/SGQAdmin/Dashboard/Redux/reducers'
import { incidents } from '../Scenes/SGQAdmin/Incidents/Redux/reducer'
import { consultanciesAndAdvisory } from '../Scenes/SGQAdmin/Compliance/Redux/reducer'

export const reducers = combineReducers({
  profile,
  authenticateUser,
  forgotPassword,
  resetPassword,
  dashboard,
  incidents,
  consultanciesAndAdvisory
})
