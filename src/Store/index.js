import { createStore, applyMiddleware, compose } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage/session' // defaults to localStorage for web

import thunk from 'redux-thunk'
import { reducers } from './reducers'

const persistConfig = {
  key: 'root',
  storage,
}

const persistedReducer = persistReducer(persistConfig, reducers)

export const configureStore = () => {
  let store = createStore(persistedReducer, compose(applyMiddleware(thunk)))
  let persistor = persistStore(store)

  return  { store, persistor }
}

export default configureStore
