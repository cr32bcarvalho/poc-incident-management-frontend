export const AUTH_PATH = '/auth/'
export const ADM_PATH = '/sgq/'
export const INCIDENTS_PATH = `${ADM_PATH}incidents/`
export const CONSULTANCIES_PATH = `${ADM_PATH}consultancies_and_advisory/`
    
const paths = {
  SIGN_IN: `${AUTH_PATH}sign-in`,
  LOGOUT: `${ADM_PATH}logout`,
  FORGOT_PASSWORD: `${AUTH_PATH}forgot-password`,
  RESET_PASSWORD: `${AUTH_PATH}reset-password`,
  DASHBOARD: `${ADM_PATH}`,
  INCIDENTS: `${INCIDENTS_PATH}`,
  CONSULTANCIES: `${CONSULTANCIES_PATH}`,
}

export default paths