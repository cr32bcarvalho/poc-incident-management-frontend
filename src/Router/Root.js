import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Router, Switch, withRouter } from 'react-router-dom'
import { AuthRoute, AdminRoute } from './Routes'

export const Root = ({loggedIn}) => {
  const props= {
    loggedIn
  }
  return(
    <>
      <AuthRoute  {...props} />
      <AdminRoute {...props} />
    </>
  )
}

class ReduxRouter extends Component {
  render() {
    const { user, token } = this.props.profile
    const loggedIn = (user !== null && token && token.id !== null && user.id != null ? true : false)
    return (
      <Router history={this.props.history}>
        <Switch>
          <Root loggedIn={loggedIn} />
        </Switch>
      </Router>
    )
  }
}

const mapAuthStateProps = (state) => (
  {
    profile: state.profile
  }
)

export default withRouter(connect(mapAuthStateProps)(ReduxRouter))