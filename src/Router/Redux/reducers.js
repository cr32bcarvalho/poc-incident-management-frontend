import { Root } from '../Root'
import { withRouter } from 'react-router-dom'

export default withRouter(Root)
