import storage from 'redux-persist/lib/storage/session' // defaults to localStorage for web
import { USER_ID, USER_NAME, USER_TOKEN } from '../../Commons/Constants/Async'
import { push } from 'react-router-redux'

export const logout = () => {
  return async (dispatch) => {
    let keys = [USER_ID, USER_NAME, USER_TOKEN]
    storage.multiRemove(keys, () => {
      dispatch(push('/sign-in'))
    })
  }
}

export const goToScreen = (routeName) => {
  return async (dispatch) => {
    dispatch(push({ routeName }))
  }
}