import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import SignInScreen from '../Scenes/Auth/SignIn'
import LogoutScreen from '../Scenes/Auth/Logout'
import ForgotPassword from '../Scenes/Auth/ForgotPassword'
import ResetPassword from '../Scenes/Auth/ResetPassword'
import { Dashboard, IncidentsList, IncidentCreate, IncidentView, IncidentAprove, ConsultanciesList, ConsultanciesView } from '../Scenes/SGQAdmin'
import Paths, { ADM_PATH, INCIDENTS_PATH, CONSULTANCIES_PATH } from './paths'

const PrivateRoute = ({ component: Comp, loggedIn, path, ...rest }) => {
  return (
    <Route
      path={path}
      {...rest}
      render={(props) => {
        return loggedIn ? (
          <Comp {...props} />
        ) : (
          <Redirect
            to={{
              pathname: Paths.SIGN_IN,
              state: {
                prevLocation: path,
                error: 'You need to login first!',
              },
            }}
          />
        )
      }}
    />
  )
}

export const AuthRoute = (loggedIn) => 
    <>
      <Route exact path="/">
        {loggedIn ? <Redirect to={ADM_PATH} /> : <Redirect to={Paths.SIGN_IN} />}
      </Route>
      <Route exact path={Paths.SIGN_IN} component={SignInScreen} />
      <Route exact path={Paths.FORGOT_PASSWORD} component={ForgotPassword} />
      <Route exact path={Paths.RESET_PASSWORD}  component={ResetPassword} />
    </>

export const AdminRoute = ({loggedIn}) => 
    <>
      <Route exact path={Paths.LOGOUT} component={LogoutScreen} />
      <PrivateRoute loggedIn={loggedIn} exact path={ADM_PATH} component={Dashboard} />
      <PrivateRoute loggedIn={loggedIn} exact path={INCIDENTS_PATH} forceRefresh={true} component={IncidentsList} />
      <PrivateRoute loggedIn={loggedIn} exact path={`${INCIDENTS_PATH}:id/show`} component={IncidentView} />
      <PrivateRoute loggedIn={loggedIn} exact path={`${INCIDENTS_PATH}create`} component={IncidentCreate} />
      <PrivateRoute loggedIn={loggedIn} exact path={`${INCIDENTS_PATH}:id/update`} component={IncidentCreate} />
      <PrivateRoute loggedIn={loggedIn} exact path={`${INCIDENTS_PATH}:id/aprove`} component={IncidentAprove} />
      <PrivateRoute loggedIn={loggedIn} exact path={CONSULTANCIES_PATH} forceRefresh={true} component={ConsultanciesList} />
      <PrivateRoute loggedIn={loggedIn} exact path={`${CONSULTANCIES_PATH}:id/show`} forceRefresh={true} component={ConsultanciesView} />
    </>


