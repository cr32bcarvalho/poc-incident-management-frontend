import React, { Component } from 'react'
import { Provider } from 'react-redux'

import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import { PersistGate } from 'redux-persist/integration/react'

import CssBaseline from '@material-ui/core/CssBaseline'
import { ThemeProvider } from '@material-ui/core/styles'

import { configureStore } from './Store'

import  Loading  from './Commons/Components/Loading'

import AppRouter from './Router/Root'
import theme from './theme'

const persistorStore = configureStore()
const history = createBrowserHistory()

export default class ReduxApp extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true
    }
  }

  async componentDidMount() {
    this.bootstrapAsync()
  }

  componentWillUnmount = () =>{
    this.bootstrapAsync = false
  }

  bootstrapAsync = async () => {
    this.setState({ isLoading: false })
  }

  renderLoading = () => <Loading open={true}/>

  renderStash = () => <AppRouter />

  render() {
    const { isLoading } = this.state
    const { store, persistor } = persistorStore
    return (
      <Provider store={store}>
        <PersistGate loading={<Loading />} persistor={persistor}>
          <Router history={history} forceRefresh={true}>
            <ThemeProvider theme={theme}>
              <CssBaseline />
              {!isLoading && this.renderStash()}
              {isLoading && this.renderLoading()}
            </ThemeProvider>         
          </Router>
        </PersistGate>
      </Provider>
    )
  }
}
